
public class Test_Java {

	public static void main(String[] args) {
		System.out.println("-- Hello World --");

		Fruit apple = new Fruit("Apple", "red", "sweet");
		apple.printMsg();

		Fruit mango = new Fruit("Mango", "yellow", "sour");
		mango.printMsg();


		NewClass1 newClass1 = new NewClass1();
		newClass1.printMsg();
	}

}


class Fruit {

	String name;
	String colour;
	String taste;

	public Fruit(String name, String colour, String taste) {
		this.name = name;
		this.colour = colour;
		this.taste = taste;
	}

	public void printMsg() {
		System.out.println("");
		System.out.println("-- Fruit --" + this.name);
		System.out.println("-- colour --" + this.colour);
		System.out.println("-- taste --" + this.taste);
		System.out.println("");
	}

}

